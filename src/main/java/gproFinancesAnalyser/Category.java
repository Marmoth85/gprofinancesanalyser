package gproFinancesAnalyser;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class Category {
	
	// Code de la catégorie
	private String code;
	// Nom de la catégorie
	private String name;
	// Somme enregistrée de la catégorie
	private Integer amount;
	// Les strings à rechercher pour récupérer les opérations
	private List<String> tags;
	// Liste des opérations taggués par la catégorie
	private List<Operation> operations;
	// Niveau de la catégorie
	private int level;
	// Catégories filles
	private List<Category> ownedCategories;
	
	
	public Category() {}
	
	
	public Category(final String name) {
		final String[] tmp = name.split(" - ");
		if (tmp != null && tmp.length == 2) {
			this.code = tmp[0];
			this.name = tmp[1];
		}
		amount = 0;
		tags = new ArrayList<>();
		evaluateLevel();
		operations = new ArrayList<>();
		ownedCategories = new ArrayList<>();
	}
	
	
	public void evaluateLevel() {
		if (StringUtils.isNotBlank(code)) {
			
			if (code.length() == 1) {
				level = 1;
			} else {
			
				final String[] tmp = code.split("\\.");
				if (tmp != null) {
					level = tmp.length;
				} else {
					level = 0;
				}
				
			}
		} else {
			level = 0;
		}
	}
	
	
	public void evaluateAmount() {
		
		if (tags == null || ownedCategories == null) {
			return;
		}
		
		if (!tags.isEmpty() && ownedCategories.isEmpty()) {
			if (operations == null || operations.isEmpty()) {
				amount = 0;
				return;
			} else {
				amount = 0;
				for (Operation ope : operations) {
					amount += ope.getAmount();
				}
			}
		}
		
		if (tags.isEmpty() && !ownedCategories.isEmpty()) {
			amount = 0;
			for (Category cat : ownedCategories) {
				amount += cat.getAmount();
			}
		}
	}
	
	
	public void addTag(final String tag) {
		tags.add(tag);
	}
	
	
	public String getCodeParent() {
		String result = null;
		
		if (level > 1) {
			result = code.substring(0, code.length() - 2);
		}
		
		return result;
	}
	
	
	public void addOperation(final Operation ope) {
		operations.add(ope);
	}
	
	
	public void addSubCategory(final Category cat) {
		ownedCategories.add(cat);
	}
	
	
	public String toString() {
		return code + " - " + name;
	}
	
	
	public String getCode() {
		return code;
	}

	
	public String getName() {
		return name;
	}

	
	public Integer getAmount() {
		return amount;
	}

	
	public List<String> getTags() {
		return tags;
	}

	
	public List<Operation> getOperations() {
		return operations;
	}

	
	public int getLevel() {
		return level;
	}

	
	public List<Category> getOwnedCategories() {
		return ownedCategories;
	}
}