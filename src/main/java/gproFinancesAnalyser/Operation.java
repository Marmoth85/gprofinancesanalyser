package gproFinancesAnalyser;

import org.apache.commons.lang3.StringUtils;

public class Operation {

	private String name;
	private String strAmount;
	private int amount;
	
	
	public Operation() {
	}
	
	
	public void evaluateAmount() {
		if (StringUtils.isNotBlank(strAmount)) {
			String[] tmp = strAmount.split("\\$");
			if (tmp != null && tmp.length == 2) {
				String money = tmp[1];
				if (StringUtils.isNotBlank(money)) {
					tmp = money.split("\\.");
					if (tmp != null && tmp.length > 0) {
						String moneyStrFormatted = "";
						for (String str : tmp) {
							moneyStrFormatted += str;
						}
						amount = Integer.valueOf(moneyStrFormatted);
					}
				}
			}
		}
	}
	
	
	public String toString() {
		return strAmount + " - " + name;
	}
	
	
	public String getName() {
		return name;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getStrAmount() {
		return strAmount;
	}
	
	
	public void setStrAmount(String strAmount) {
		this.strAmount = strAmount;
	}
	
	
	public int getAmount() {
		return amount;
	}
}