package gproFinancesAnalyser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class FinancesAnalyser {
	
	private static final String STR_OUTPUT_PATH_FILENAME_CSV = "E:\\GPRO\\Bilan\\S81\\Bilan.csv";
	private static final String STR_CONFIG_PATH_FILENAME_TXT = "E:\\GPRO\\Bilan\\AnalyseFinancesGPRO.txt";
	private static final String STR_ECONOMY_HISTORY_PATH_FILENAME_CSV = "E:\\GPRO\\Bilan\\S81\\EconomyHistory.csv";
	
	private String financesFileName;
	private String categoriesFileName;
	private String outputFileName;
	
	private List<Category> categories;
	
	
	public FinancesAnalyser() {}
	
	
	public void process() throws IOException {
		// Chemins hard codés pour plus de rapidité : à modifier plus tard
		setFinancesFileName(STR_ECONOMY_HISTORY_PATH_FILENAME_CSV);
		setCategoriesFileName(STR_CONFIG_PATH_FILENAME_TXT);
		setOutputFileName(STR_OUTPUT_PATH_FILENAME_CSV);
		
		readConfigFile();
		findCategoryDependencies();
		
		readFinancesFile();
		generateOutputFile();
	}
	
	
	public void findCategoryDependencies() {
		for (int i = 0; i < categories.size(); i++) {
			final Category current = categories.get(i);
			final String codeParent = current.getCodeParent();
			if (codeParent != null) {
				final Category parent = searchCategoryByCode(codeParent);
				parent.addSubCategory(current);
//				System.out.println("La catégorie : " + current.getName() + " a été ajoutée à la rubrique " + parent.getCode());
			}
		}
	}
	
	
	public Category searchCategoryByCode(final String code) {
		Category result = null;
		
		for (int i = 0; i < categories.size(); i++) {
			final String str = categories.get(i).getCode();
			if (code.equals(str)) {
				result = categories.get(i);
				break;
			}
		}
		
		return result;
	}
	
	
	public Category searchCategoryFromOperation(final Operation oper) {
		
		for(final Category cat : categories) {
			
			final List<String> tags = cat.getTags();
			if (tags.isEmpty()) {
				continue;
			}
			
			for (final String tag : tags) {
				if (oper.getName().contains(tag)) {
					return cat;
				}
			}
		}
		
		return null;
	}
	
	
	private void evaluateAmountOfCategory(final Category owningCategory) {
		owningCategory.evaluateAmount();
		final String codeParent = owningCategory.getCodeParent();
		if (StringUtils.isNotBlank(codeParent)) {
			final Category parent = searchCategoryByCode(codeParent);
			evaluateAmountOfCategory(parent);
		}
	}
	
	
	public void generateOutputFile() throws IOException {
		BufferedWriter bw = null;
		String ligne = "";
		try {
			bw = new BufferedWriter(new FileWriter(outputFileName, false));
			
			ligne = "Code;Catégorie;Montant;";
			bw.write(ligne);
			bw.newLine();
			
			for(Category cat : categories) {
				ligne = cat.getCode() + ";" + cat.getName() + ";" + Math.abs(cat.getAmount()) + ";";
				bw.write(ligne);
				bw.newLine();
//				for (Operation oper : cat.getOperations()) {
//					ligne = ";" + oper.getName() + ";" + oper.getAmount() + ";";
//					bw.write(ligne);
//					bw.newLine();
//				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			bw.close();
		}
	}
	
	
	public void readFinancesFile() throws IOException {
		BufferedReader br = null;
		String ligne = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(financesFileName), "UTF-8"));
			ligne = br.readLine();
			while(ligne != null) {
				if (!ligne.equals("Vous pouvez voir ici les dernières transactions (débit & crédit) faites depuis votre compte;;;;")
						&& !ligne.equals("Date et heure;Description de la transaction;Balance avant la transaction;Montant;Balance après la transaction")) {
					final String [] tmp = ligne.split(";");
					if (tmp.length == 5) {
						
						for (int i = 0; i < tmp.length; i++) {
							if (tmp[i] != null) {
								tmp[i] = tmp[i].trim();
							}
						}
						
						final String name = tmp[1];
						final String amount = tmp[3];
						
						final Operation oper = new Operation();
						oper.setName(name);
						oper.setStrAmount(amount);
						oper.evaluateAmount();
						
						final Category owningCategory = searchCategoryFromOperation(oper);
						if (owningCategory != null) {
							owningCategory.addOperation(oper);
							evaluateAmountOfCategory(owningCategory);
						} else {
							// System.out.println("[DEBUG] : " + ligne);
							System.out.println("Non répertorié : " + oper.toString());
							System.out.println("Veuillez ajouter une rubrique dans le fichier de configuration pour cette opération");
						}
					}
//				} else {
//					 System.out.println("[DEBUG] : On ne traite pas la ligne -> " + ligne);
				}
				ligne = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
	}
	
	
	public void readConfigFile() throws IOException {
		BufferedReader br = null;
		String ligne = null;
		categories = new ArrayList<>();
		
		try {
			br = new BufferedReader(new FileReader(categoriesFileName));
			ligne = br.readLine();
			boolean isNewCategory = true;
			Category cat = new Category();
			
			while (ligne != null) {
				
				if (isNewCategory) {
					cat = new Category(ligne);
					isNewCategory = false;
				} else {
					if (! ligne.equals("")) {
						cat.addTag(ligne);
					}
				}
				if (ligne.equals("")) {
					isNewCategory = true;
					categories.add(cat);
				}
				ligne = br.readLine();
			}
			categories.add(cat);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
	}
	
	
	public static void main (String[] args) throws IOException {
		final FinancesAnalyser analyser = new FinancesAnalyser();
		analyser.process();
	}
	
	
	public String getFinancesFileName() {
		return financesFileName;
	}

	
	public void setFinancesFileName(String fileName) {
		this.financesFileName = fileName;
	}
	
	
	public String getCategoriesFileName() {
		return categoriesFileName;
	}

	
	public void setCategoriesFileName(String categoriesFileName) {
		this.categoriesFileName = categoriesFileName;
	}
	
	
	public String getOutputFileName() {
		return outputFileName;
	}

	
	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}
}